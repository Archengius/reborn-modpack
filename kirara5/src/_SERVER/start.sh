#!/bin/bash
MEMORY_GB="4"
JAVA="java"
ROOT_DIR="$(dirname $0)"

if [ -n "$JAVA_HOME" ]
  then
    JAVA="$JAVA_HOME/bin/java"
fi

if [ -z "$SERVER_JAR" ]
  then
    cd $ROOT_DIR
    for file in $(ls -1 | egrep "^forge-.*-universal.jar$")
    do
      SERVER_JAR="$file"
    done
fi

$JAVA -jar "$SERVER_JAR" -Xmx${MEMORY_GB}G -Xms${MEMORY_GB}G -d64 -XX:+DisableExplicitGC -XX:+UseConcMarkSweepGC -XX:MaxGCPauseMillis=40 -XX:CMSInitiatingOccupancyFraction=70 -XX:+CMSScavengeBeforeRemark -XX:+AggressiveOpts $@ nogui
