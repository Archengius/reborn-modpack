//created by keda

val Furnace = <minecraft:furnace>;


<ore:cobblestone>.add(<kirarautils:tile.cobble1:0>);
<ore:cobblestone>.add(<kirarautils:tile.cobble1:1>);
<ore:cobblestone>.add(<kirarautils:tile.cobble1:2>);
<ore:cobblestone>.add(<kirarautils:tile.cobble1:3>);
<ore:cobblestone>.add(<kirarautils:tile.cobble1:4>);
<ore:cobblestone>.add(<kirarautils:tile.cobble1:5>);
<ore:cobblestone>.add(<kirarautils:tile.cobble1:6>);
<ore:cobblestone>.add(<kirarautils:tile.cobble1:7>);
<ore:cobblestone>.add(<kirarautils:tile.cobble1:8>);
<ore:cobblestone>.add(<kirarautils:tile.cobble1:9>);

<ore:stoneCobble>.add(<kirarautils:tile.cobble1:0>);
<ore:stoneCobble>.add(<kirarautils:tile.cobble1:1>);
<ore:stoneCobble>.add(<kirarautils:tile.cobble1:2>);
<ore:stoneCobble>.add(<kirarautils:tile.cobble1:3>);
<ore:stoneCobble>.add(<kirarautils:tile.cobble1:4>);
<ore:stoneCobble>.add(<kirarautils:tile.cobble1:5>);
<ore:stoneCobble>.add(<kirarautils:tile.cobble1:6>);
<ore:stoneCobble>.add(<kirarautils:tile.cobble1:7>);
<ore:stoneCobble>.add(<kirarautils:tile.cobble1:8>);
<ore:stoneCobble>.add(<kirarautils:tile.cobble1:9>);
<ore:oreOilsands>.add(<PFAAGeologica:crudeSand>);
<ore:oreIron>.remove(<SGU:BandedIron>);
<ore:oreAnyIron>.remove(<SGU:BandedIron>);



furnace.setFuel(<SGU:AlumShale>, 0);

furnace.addRecipe(<SGU:AlumShale>, <kirarautils:tile.cobble1:0>);
furnace.addRecipe(<SGU:Andesite>, <kirarautils:tile.cobble1:1>);
furnace.addRecipe(<SGU:Diorite>, <kirarautils:tile.cobble1:2>);
furnace.addRecipe(<SGU:Gabbro>, <kirarautils:tile.cobble1:3>);
furnace.addRecipe(<SGU:Gneiss>, <kirarautils:tile.cobble1:4>);
furnace.addRecipe(<SGU:Granite>, <kirarautils:tile.cobble1:5>);
furnace.addRecipe(<SGU:Kimberlite>, <kirarautils:tile.cobble1:6>);
furnace.addRecipe(<SGU:Limestone>, <kirarautils:tile.cobble1:7>);
furnace.addRecipe(<SGU:Marble>, <kirarautils:tile.cobble1:8>);
furnace.addRecipe(<SGU:Shale>, <kirarautils:tile.cobble1:9>);
//Furnace.addRecipe(Iron * 2, tile.oreGold);

recipes.remove(<StorageDrawers:upgrade:6>);
recipes.remove(<StorageDrawers:upgrade:5>);
recipes.remove(<StorageDrawers:upgrade:4>);
recipes.remove(<StorageDrawers:upgrade:3>);
recipes.remove(<StorageDrawers:upgrade:2>);
recipes.addShaped(<StorageDrawers:upgrade:6>, [[<ore:plateAnyIron>, <ore:plateAnyIron>, <ore:plateAnyIron>], [<ore:plateAnyIron>, <ore:blockCoil>, <ore:plateAnyIron>], [<ore:plateAnyIron>, <ore:circuitMaster>, <ore:plateAnyIron>]]);
recipes.addShaped(<StorageDrawers:upgrade:5>, [[<ore:plateAnyIron>, <ore:plateAnyIron>, <ore:plateAnyIron>], [<ore:plateAnyIron>, <ore:coilTitanium>, <ore:plateAnyIron>], [<ore:plateAnyIron>, <ore:circuitData>, <ore:plateAnyIron>]]);
recipes.addShaped(<StorageDrawers:upgrade:4>, [[<ore:plateAnyIron>, <ore:plateAnyIron>, <ore:plateAnyIron>], [<ore:plateAnyIron>, <libVulpes:libVulpescoil0:9>, <ore:plateAnyIron>], [<ore:plateAnyIron>, <ore:circuitAdvanced>, <ore:plateAnyIron>]]);
recipes.addShaped(<StorageDrawers:upgrade:3>, [[<ore:plateAnyIron>, <ore:plateAnyIron>, <ore:plateAnyIron>], [<ore:plateAnyIron>, <ore:coilGold>, <ore:plateAnyIron>], [<ore:plateAnyIron>, <ore:circuitAdvanced>, <ore:plateAnyIron>]]);
recipes.addShaped(<StorageDrawers:upgrade:2>, [[<ore:plateAnyIron>, <ore:plateAnyIron>, <ore:plateAnyIron>], [<ore:plateAnyIron>, null, <ore:plateAnyIron>], [<ore:plateAnyIron>, <ore:circuitBasic>, <ore:plateAnyIron>]]);
recipes.addShaped(<TConstruct:CraftedSoil:1> * 2, [[<minecraft:clay_ball>, <ore:sand>], [<ore:dustStone>, <minecraft:gravel>]]);
recipes.addShaped(<SGU:AlumShale>, [[<ore:stoneCobble>]]);
//stonedust
recipes.addShapeless(<IC2:itemDust:9>,[<ore:craftingToolMortar>,<ore:stoneCobble>]);
recipes.addShapeless(<IC2:itemDust:9>,[<ore:craftingToolMortar>,<ore:stone>]);
//grout
recipes.remove(<TConstruct:CraftedSoil:1>);
recipes.addShapeless(<TConstruct:CraftedSoil:1> * 2, [
	<ore:dustStone>, 
<minecraft:gravel>,
<minecraft:clay_ball>,
<ore:sand>]);


//harvestcraft, sinks
recipes.remove(<harvestcraft:sink:0>);
recipes.remove(<harvestcraft:sink:1>);
recipes.remove(<harvestcraft:sink:2>);
recipes.remove(<harvestcraft:sink:3>);

//Tinkers tool forge nerf
recipes.remove(<TConstruct:CraftingSlab:5>);

recipes.remove(<IC2:itemPartIndustrialDiamond>);

//Convert regular slimeballs to blue slimeballs and also blue slimeballs back to regular slimeballs
recipes.addShapeless(<TConstruct:strangeFood>, [<minecraft:slime_ball>]);
recipes.addShapeless(<minecraft:slime_ball>, [<TConstruct:strangeFood>]);

recipes.remove(<Magneticraft:mg_tank> * 2);
recipes.remove(<TConstruct:knapsack>);

