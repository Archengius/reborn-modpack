// --- Created by Zulfurlubak for KR ---

// --- Variables ---

val IronTrapdoor = <malisisdoors:iron_trapdoor>;
val SlidingTrapdoor = <malisisdoors:sliding_trapdoor>;
val PlayerSensor = <malisisdoors:player_sensor>;
val BlockMixer = <malisisdoors:block_mixer>;
val DoorFactory = <malisisdoors:door_factory>;
val RustyHatch = <malisisdoors:rustyHatch>;
val RustyLadder = <malisisdoors:rustyLadder>;
val WoodenGlassDoor = <malisisdoors:item.wood_sliding_door>;
val IronGlassDoor = <malisisdoors:item.iron_sliding_door>;
val JailDoor = <malisisdoors:item.jail_door>;
val LaboratoryDoor = <malisisdoors:item.laboratory_door>;
val FactoryDoor = <malisisdoors:item.factory_door>;
val ShojiDoor = <malisisdoors:item.shoji_door>;

val RustyHandle = <malisisdoors:item.rustyHandle>;
val ForcefieldController = <malisisdoors:item.forcefieldItem>;
var EnergyCrystal = <IC2:itemBatCrystal>.anyDamage();

val HHammer = <ore:craftingToolHardHammer>;
val Wrench = <ore:craftingToolWrench>;
val Screwdriver = <ore:craftingToolScrewdriver>;


// --- Removing Recipes ---


// --- Blocks ---


// --- Iron Trapdoor
recipes.remove(IronTrapdoor);
// --- Sliding Trapdoor
recipes.remove(SlidingTrapdoor);
// --- Player Sensor
recipes.remove(PlayerSensor);
// --- Block Mixer
recipes.remove(BlockMixer);
// --- Door Factory
recipes.remove(DoorFactory);
// --- Rusty Hatch
recipes.remove(RustyHatch);
// --- Rusty Ladder
recipes.remove(RustyLadder);
// --- Wooden Glass Door
recipes.remove(WoodenGlassDoor);
// --- Iron Glass Door
recipes.remove(IronGlassDoor);
// --- Jail Door
recipes.remove(JailDoor);
// --- Laboratory Door
recipes.remove(LaboratoryDoor);
// --- Factory Door
recipes.remove(FactoryDoor);
// --- Shoji Door
recipes.remove(ShojiDoor);


// --- Items ---


// --- Rusty Handle
recipes.remove(RustyHandle);
// --- Forcefield Controller
recipes.remove(ForcefieldController);


// --- Adding Back Recipes ---


// --- Blocks ---


// --- Iron Trapdoor
recipes.addShaped(IronTrapdoor, [
[<ore:plateAnyIron>, <ore:plateAnyIron>],
[<ore:plateAnyIron>, <ore:plateAnyIron>],
[HHammer, null]]);

// --- Sliding Trapdoor
recipes.addShaped(SlidingTrapdoor, [
[<ore:plateGold>, <ore:plateAnyIron>, <ore:plateAnyIron>],
[<ore:plateGold>, <ore:plateAnyIron>, <ore:plateAnyIron>],
[HHammer, null, null]]);

// --- Player Sensor
recipes.addShaped(PlayerSensor, [
[<IC2:itemCasing:4>, <gregtech:gt.metaitem.01:32690>, <IC2:itemCasing:4>],
[<ore:paneGlassColorless>, <ore:paneGlassColorless>, <ore:paneGlassColorless>]]);

// --- Block Mixer
recipes.addShaped(BlockMixer, [
[<ore:plateIron>, <ore:plateIron>, <ore:plateIron>],
[<minecraft:piston>, Wrench, <minecraft:piston>],
[<ore:plateIron>, <ore:plateIron>, <ore:plateIron>]]);

// --- Door Factory
recipes.addShaped(DoorFactory, [
[<ore:plateIron>, <minecraft:iron_door>, <ore:plateIron>],
[<ore:circuitBasic>, Wrench, <ore:circuitBasic>],
[<ore:plateIron>, <minecraft:piston>, <ore:plateIron>]]);

// --- Rusty Hatch
recipes.addShaped(RustyHatch, [
[<ore:plateAnyIron>, <ore:plateAnyIron>, <ore:plateAnyIron>],
[<ore:plateAnyIron>, <malisisdoors:item.rustyHandle>, <ore:plateAnyIron>],
[<ore:plateAnyIron>, <ore:plateAnyIron>, <ore:plateAnyIron>]]);

// --- Rusty Ladder
recipes.addShaped(RustyLadder * 3, [
[<ore:stickAnyIron>, <ore:screwAnyIron>, <ore:stickAnyIron>],
[<ore:stickAnyIron>, <ore:stickAnyIron>, <ore:stickAnyIron>],
[<ore:stickAnyIron>, Screwdriver, <ore:stickAnyIron>]]);

// --- Wooden Glass Door
recipes.addShaped(WoodenGlassDoor, [
[<ore:plankWood>, <ore:paneGlassColorless>, <ore:stickWood>],
[<ore:plankWood>, <ore:paneGlassColorless>, <ore:stickWood>],
[<ore:plankWood>, <ore:paneGlassColorless>, <ore:stickWood>]]);

// --- Iron Glass Door
recipes.addShaped(IronGlassDoor, [
[<ore:plateAnyIron>, <ore:paneGlassColorless>, <ore:stickAnyIron>],
[<ore:plateAnyIron>, <ore:paneGlassColorless>, <ore:stickAnyIron>],
[<ore:plateAnyIron>, <ore:paneGlassColorless>, <ore:stickAnyIron>]]);

// --- Jail Door
recipes.addShaped(JailDoor, [
[<minecraft:iron_bars>, <minecraft:iron_bars>, <ore:stickLongAnyIron>],
[<minecraft:iron_bars>, <minecraft:iron_bars>, <ore:screwAnyIron>],
[<minecraft:iron_bars>, <minecraft:iron_bars>, Screwdriver]]);

// --- Laboratory Door
recipes.addShaped(LaboratoryDoor, [
[<ore:plateGold>, <ore:plateGold>, null],
[<ore:plateAnyIron>, <ore:plateAnyIron>, HHammer],
[<ore:plateAnyIron>, <ore:plateAnyIron>, null]]);

// --- Factory Door
recipes.addShaped(FactoryDoor, [
[<ore:plateGold>, <ore:plateGold>, null],
[<ore:plateAnyIron>, <ore:plateAnyIron>, HHammer],
[<ore:plateGold>, <ore:plateGold>, null]]);

// --- Shoji Door
recipes.addShaped(ShojiDoor, [
[<ore:plankWood>, <minecraft:paper>, <ore:stickWood>],
[<ore:plankWood>, <minecraft:paper>, <ore:stickWood>],
[<ore:plankWood>, <minecraft:paper>, <ore:stickWood>]]);


// --- Items ---


// --- Rusty Handle
recipes.addShaped(RustyHandle, [
[null, <ore:ringAnyIron>, null],
[HHammer, <ore:stickLongAnyIron>, Wrench],
[null, <ore:ringAnyIron>, null]]);

// --- Forcefield Controller
recipes.addShaped(ForcefieldController, [
[<ore:stickChrome>, <ore:lensDiamond>, <ore:stickChrome>],
[<ore:circuitMaster>, <gregtech:gt.metaitem.01:32682>, <ore:circuitMaster>],
[<ore:plateStainlessSteel>, EnergyCrystal, <ore:plateStainlessSteel>]]);
