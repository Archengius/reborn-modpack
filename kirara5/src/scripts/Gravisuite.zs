
// --- Created by Kosya

// --- Modified by OrdoFree

val gtSuperConductor = <ore:wireGt01Superconductor>;

val iridiumPlate = <ore:plateAlloyIridium>;

val tsDrillHead = <ore:toolHeadDrillTungstenSteel>;

val tsChainsawHead = <ore:toolHeadChainsawTungstenSteel>;


val gsSuperConductor = <GraviSuite:itemSimpleItem:1>;



// Superconductor cover
recipes.remove(<GraviSuite:itemSimpleItem>);


// Superconductor
recipes.remove(gsSuperConductor);

recipes.addShaped(gsSuperConductor, [
[gtSuperConductor, gtSuperConductor, gtSuperConductor],

[gtSuperConductor, iridiumPlate, gtSuperConductor],

[gtSuperConductor, gtSuperConductor, gtSuperConductor]]);

// Drill
recipes.remove(<GraviSuite:advDDrill>);

recipes.addShaped(<GraviSuite:advDDrill>, 
[
[<gregtech:gt.metaitem.02:8374>,<gregtech:gt.metaitem.02:8374>, <gregtech:gt.metaitem.02:8374>],

[<IC2:upgradeModule>, <IC2:itemToolDDrill>, <IC2:upgradeModule>],

[<ore:circuitAdvanced>, <IC2:upgradeModule>, <ore:circuitAdvanced>]]);



// Chainsaw
recipes.remove(<GraviSuite:advChainsaw>);

recipes.addShaped(<GraviSuite:advChainsaw>, 
[
[tsChainsawHead, tsChainsawHead, tsChainsawHead],

[<IC2:upgradeModule>, <IC2:itemToolChainsaw:1>, <IC2:upgradeModule>],

[<ore:circuitAdvanced>, <IC2:upgradeModule>, <ore:circuitAdvanced>]]);


// --- Created by DreamMasterXXL
// --- Modified by OrdoFree

// ---
recipes.remove(<GraviSuite:relocator:27>);
// --- Super Conductor
recipes.addShaped(<GraviSuite:itemSimpleItem:1>, [
[<GraviSuite:itemSimpleItem>, <GraviSuite:itemSimpleItem>, <GraviSuite:itemSimpleItem>],
[<gregtech:gt.blockmachines:2024>, <ore:platePlatinum>, <gregtech:gt.blockmachines:2024>],
[<GraviSuite:itemSimpleItem>, <GraviSuite:itemSimpleItem>, <GraviSuite:itemSimpleItem>]]);

// --- Cooling Core
recipes.addShaped(<GraviSuite:itemSimpleItem:2>, [
[<IC2:reactorVentDiamond:1>, <IC2:reactorHeatSwitchDiamond:1>, <IC2:reactorVentDiamond:1>],
[<gregtech:gt.180k_NaK_Coolantcell>, <ore:plateAlloyIridium>, <gregtech:gt.180k_NaK_Coolantcell>],
[<IC2:reactorPlatingHeat>, <IC2:reactorHeatSwitchDiamond:1>, <IC2:reactorPlatingHeat>]]);
// -
recipes.addShaped(<GraviSuite:itemSimpleItem:2>, [
[<IC2:reactorVentDiamond:1>, <IC2:reactorHeatSwitchDiamond:1>, <IC2:reactorVentDiamond:1>],
[<gregtech:gt.180k_Helium_Coolantcell>, <ore:plateAlloyIridium>, <gregtech:gt.180k_Helium_Coolantcell>],
[<IC2:reactorPlatingHeat>, <IC2:reactorHeatSwitchDiamond:1>, <IC2:reactorPlatingHeat>]]);

// --- Gravitation Engine
recipes.addShaped(<GraviSuite:itemSimpleItem:3>, [
[<IC2:blockMachine2:1>, <GraviSuite:itemSimpleItem:1>, <IC2:blockMachine2:1>],
[<GraviSuite:itemSimpleItem:2>, <gregtech:gt.blockmachines:23>, <GraviSuite:itemSimpleItem:2>],
[<IC2:blockMachine2:1>, <GraviSuite:itemSimpleItem:1>, <IC2:blockMachine2:1>]]);
 
// --- Magnetron
recipes.addShaped(<GraviSuite:itemSimpleItem:4>, [
[<ore:plateDenseNeodymiumMagnetic>, <IC2:itemRecipePart>, <ore:plateDenseNeodymiumMagnetic>],
[<ore:plateDenseCopper>, <GraviSuite:itemSimpleItem:1>, <ore:plateDenseCopper>],
[<ore:plateDenseNeodymiumMagnetic>, <IC2:itemRecipePart>, <ore:plateDenseNeodymiumMagnetic>]]);
 
// --- Vajra Core
recipes.addShaped(<GraviSuite:itemSimpleItem:5>, [
[<ore:craftingToolWrench>, <ore:plateCopper>, <ore:craftingToolHardHammer>],
[<IC2:itemPartIridium>, <IC2:blockMachine2:1>, <IC2:itemPartIridium>],
[<GraviSuite:itemSimpleItem:1>, <gregtech:gt.blockmachines:23>, <GraviSuite:itemSimpleItem:1>]]);

// --- Vajra
recipes.addShaped(<GraviSuite:vajra:27>, [
[<gregtech:gt.metaitem.01:32673>, <GraviSuite:itemSimpleItem:4>, <ore:lensReinforcedGlass>],
[<ore:plateAlloyCarbon>, <GraviSuite:itemSimpleItem:5>, <ore:plateAlloyCarbon>],
[<ore:plateAlloyIridium>, <ore:batteryMaster>, <ore:plateAlloyIridium>]]);
 
// --- Engine Boost
recipes.addShaped(<GraviSuite:itemSimpleItem:6>, [
[<gregtech:gt.metaitem.01:20028>, <IC2:itemPartAlloy>, <gregtech:gt.metaitem.01:20028>],
[<ore:circuitAdvanced>, <IC2:upgradeModule>, <ore:circuitAdvanced>],
[<IC2:reactorPlating>, <IC2:reactorVentDiamond:1>, <IC2:reactorPlating>]]);

// --- Gravi Chest
recipes.addShaped(<GraviSuite:graviChestPlate:27>, [
[<GraviSuite:itemSimpleItem:1>, <IC2:itemArmorQuantumChestplate:*>, <GraviSuite:itemSimpleItem:1>],
[<GraviSuite:itemSimpleItem:3>, <gregtech:gt.blockmachines:23>, <GraviSuite:itemSimpleItem:3>],
[<GraviSuite:itemSimpleItem:1>, <GraviSuite:ultimateLappack:*>, <GraviSuite:itemSimpleItem:1>]]);
 
// --- Advanced Nano Chest
recipes.addShaped(<GraviSuite:advNanoChestPlate:27>, [
[<IC2:itemPartCarbonPlate>, <GraviSuite:advJetpack:*>, <IC2:itemPartCarbonPlate>],
[<ore:plateIridium>, <IC2:itemArmorNanoChestplate:*>, <ore:plateIridium>],
[<ore:wireGt12Platinum>, <ore:circuitAdvanced>, <ore:wireGt12Platinum>]]);
 
// --- Advanced Jetpack
recipes.addShaped(<GraviSuite:advJetpack:27>, [
[<gregtech:gt.metaitem.01:20372>, <IC2:itemArmorJetpackElectric:*>, <gregtech:gt.metaitem.01:20372>],
[<GraviSuite:itemSimpleItem:6>, <GraviSuite:advLappack:*>, <GraviSuite:itemSimpleItem:6>],
[<gregtech:gt.blockmachines:1643>, <gregtech:gt.metaitem.01:32706>, <gregtech:gt.blockmachines:1643>]]);

// --- Advanced Lappack
recipes.addShaped(<GraviSuite:advLappack:27>, [
[<gregtech:gt.metaitem.01:22313>, <ore:batteryElite>, <gregtech:gt.metaitem.01:22313>],
[<ore:batteryElite>, <IC2:itemArmorEnergypack:*>, <ore:batteryElite>],
[<ore:circuitMaster>, <ore:wireGt12Nichrome> , <ore:circuitMaster>]]);

// --- Ultimate Lappack
recipes.addShaped(<GraviSuite:ultimateLappack:27>, [
[<gregtech:gt.metaitem.01:22370>, <ore:batteryMaster>, <gregtech:gt.metaitem.01:22370>],
[<ore:batteryMaster>, <GraviSuite:advLappack:*>, <ore:batteryMaster>],
[<gregtech:gt.metaitem.01:32706>, <ore:wireGt16NiobiumTitanium>, <gregtech:gt.metaitem.01:32706>]]);

// --- Relocator
recipes.addShaped(<GraviSuite:relocator:27>, [
[<ore:plateAlloyIridium>, <ore:gemNetherStar>, <ore:plateNeutronium>],
[<ore:gemEnderEye>, <ore:craftingTeleporter>, <minecraft:dragon_egg>.reuse()],
[<ore:plateNaquadah>, <gregtech:gt.metaitem.01:32674>, <ore:plateOsmium>]]);

// --- Nei Change Names ---


 
// --- SuperConductorCover
<GraviSuite:itemSimpleItem>.displayName = "Superconductor Cover";

// --- SuperConductor
<GraviSuite:itemSimpleItem:1>.displayName = "Superconductor";

// --- Cooling Core
<GraviSuite:itemSimpleItem:2>.displayName = "Cooling Core";

// --- Gravitation Engine
<GraviSuite:itemSimpleItem:3>.displayName = "Gravitation Engine";

// --- Magnetron
<GraviSuite:itemSimpleItem:4>.displayName = "Magnetron";

// --- VajraCore
<GraviSuite:itemSimpleItem:5>.displayName = "Vajra Core";

// --- EngineBoost
<GraviSuite:itemSimpleItem:6>.displayName = "Engine Booster";
